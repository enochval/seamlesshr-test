<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'text' => $faker->word,
        'code' => $faker->word,
        'name' => $faker->word,
        'another' => $faker->word,
    ];
});
