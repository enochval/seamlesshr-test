<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class)->select([
            'id', 'created_at'
        ]);
    }

    public function getCoursesWithUsers()
    {
        return $this->with('users')->paginate();
    }
}
