<?php


namespace App\Utils;


class Rules
{
    const RULES = [
        'REGISTER' => [
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:8'
        ],
        'LOGIN' => [
            'email' => 'required',
            'password' => 'required'
        ],
        'REGISTER_COURSES' => [
            'course_ids' => 'required|array'
        ]
    ];

    public static function get($rule)
    {
        return data_get(self::RULES, $rule);
    }

}
