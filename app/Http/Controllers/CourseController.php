<?php

namespace App\Http\Controllers;

use App\Course;
use App\Export\CoursesExport;
use App\Jobs\CreateCoursesJob;
use App\Utils\Rules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function queueCourses()
    {
        $this->dispatch(new CreateCoursesJob());
        return response()->json([
            "message" => "Courses are successfully added to queue"
        ]);
    }

    public function registerCourse()
    {
        $payload = request()->only('course_ids');

        $validator = Validator::make($payload, Rules::get('REGISTER_COURSES'));
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->getMessageBag()->all()
            ], 422);
        }
        $courseIDs = $payload['course_ids'];

        auth()->user()->registerCourses($courseIDs);

        return response()->json([
            "message" => "Courses registration successful"
        ]);
    }

    public function allCourses(Course $course)
    {
        $courses = $course->getCoursesWithUsers();
        return response()->json([
            "data" => $courses
        ]);
    }

    public function exportCourses()
    {
        return Excel::download(new CoursesExport(), 'Courses.csv');
    }
}
