<?php


namespace App\Http\Controllers;


use App\User;
use App\Utils\Rules;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function register(User $user)
    {
        $payload = request()->all();

        $validator = Validator::make($payload, Rules::get('REGISTER'));
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->getMessageBag()->all()
            ], 422);
        }

        $user = $user->make(Arr::only($payload, ['email', 'password', 'name']));

        if ($user) {
            return response()->json([
                'message' => 'Registration Successful'
            ]);
        }

        return response()->json([
            "error" => "Unable to register user"
        ], 400);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        $validator = Validator::make($credentials, Rules::get('LOGIN'));
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->getMessageBag()->all()
            ], 422);
        }

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
